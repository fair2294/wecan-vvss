package tasks.model;
import org.junit.jupiter.api.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test class")
public class ArrayTaskListTest {
    private static Date start;
    private static Date end;
    private static String titlu;
    private Task task;
    private static SimpleDateFormat sdf;
    final int MIN_INT = -2147483648;
    final int MAX_INT = 2147483647;
    ArrayTaskList tasks = new ArrayTaskList();
    ArrayTaskList tasks2 = new ArrayTaskList();
    ArrayTaskList tasks3 = new ArrayTaskList();

    @BeforeAll
    @DisplayName("init values used in tests")
    static void init() {
        sdf = Task.getDateFormat();
        titlu="titlu";
        try {
            start = sdf.parse("2020-02-30 12:00");
            end = sdf.parse("2020-02-03 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
    }

    @DisplayName("add method test for title")
    @Tag("add")
    @org.junit.jupiter.api.Test
    void TestAdd_withTitle() {
        tasks.add(new Task("T", start, end, 30));
        assertEquals(1, tasks.size());
        //150 caractere
        tasks.add(new Task("1123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", start, end, 30));
        assertEquals(2, tasks.size());
        tasks.add(new Task("Task Tile", start, end, 30));
        assertEquals(3, tasks.size());
        //titlu null
        tasks.add(new Task("", start, end, 30));
        assertEquals(4, tasks.size());

    }

    @Disabled
    @Tag("add")
    void TestAdd_titleOver150(){
        //151 caraactere
        tasks.add(new Task("1123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", start, end, 30));
        assertEquals(3, tasks.size());
    }


    @DisplayName("add method test for interval")
    @Tag("add")
    @Test
    void TestAdd_withInterval() {
        tasks2.add(new Task(titlu,start,end,MAX_INT));
        assertEquals(1,tasks2.size());
        tasks2.add(new Task(titlu,start,end,MAX_INT));
        assertEquals(2,tasks2.size());
    }

    @Tag("add")
    @RepeatedTest(5)
    void TestAdd_repeat(){
        tasks3.add(new Task("Title", start, end, 30));
        assertEquals(0, tasks.size());
        tasks3.remove(new Task("Title", start, end, 30));
    }
}