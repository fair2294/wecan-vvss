package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ArrayTaskListMockTest {
    private ArrayTaskList arrayTaskList;

    @BeforeEach
    void setUp(){
        arrayTaskList = new ArrayTaskList();
    }

    @Test
    void arrayTaskList_addValidTask(){
        Task taskMock = mock(Task.class);
        arrayTaskList.add(taskMock);

        assertEquals(1, arrayTaskList.size());
    }

    @Test
    void arrayTaskList_getAll(){
        Task taskMock1= mock(Task.class);
        Task taskMock2= mock(Task.class);
        Task taskMock3= mock(Task.class);
        Task taskMock4= mock(Task.class);
        Task taskMock5= mock(Task.class);

        arrayTaskList.add(taskMock1);
        arrayTaskList.add(taskMock2);

        assertEquals(2, arrayTaskList.getAll().size());

        arrayTaskList.add(taskMock3);
        arrayTaskList.add(taskMock4);
        arrayTaskList.add(taskMock5);

        assertEquals(5, arrayTaskList.getAll().size());
    }
}
