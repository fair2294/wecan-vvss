package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.services.TasksService;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrateArrayTaskListTest {
    private ArrayTaskList arrayTaskList;
    private TasksService tasksService;


    @BeforeEach
    void setUp(){
        arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(new TaskStub());
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    void integrateArrayTaskList_getObservableList(){
        assertEquals(1, tasksService.getObservableList().size());
    }

    @Test
    void integrateArrayTaskList_getIntervalInHours(){
        assertEquals("00:16", tasksService.getIntervalInHours(tasksService.getObservableList().get(0)));
    }
}
