package tasks.model;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.SortedMap;

import static org.junit.jupiter.api.Assertions.*;

public class TasksOperationsTest {
    private static Date start;
    private static Date end;
    private static Date correctDate1;
    private static Date correctDate2;
    private static Date wrongDate1;
    private static Date wrongDate2;
    private static Date endBefore;
    private Task task;
    private static SimpleDateFormat sdf;

    @BeforeAll
    @DisplayName("init values used in tests")
    static void init() {
        sdf = Task.getDateFormat();
        try {
            start = sdf.parse("2020-02-20 12:00");
            end = sdf.parse("2020-02-03 10:00");
            wrongDate1=sdf.parse("2021-02-03 10:00");
            wrongDate2=sdf.parse("2022-02-03 10:00");
            correctDate1=sdf.parse("2020-02-05 10:00");
            correctDate2=sdf.parse("2020-02-06 10:00");
            endBefore=sdf.parse("2020-02-30 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Test case 1")
    void TC_1() {
        ArrayTaskList tasks = new ArrayTaskList() ;
        TasksOperations taskop = new TasksOperations(FXCollections.observableArrayList(tasks.getAll()));
        SortedMap<Date, Set<Task>> calendar = taskop.calendar(start,end);
        assertEquals(0,calendar.size());
    }

    @Test
    @DisplayName("Test case 2")
    void TC_2() {
        ArrayTaskList tasks = new ArrayTaskList() ;
        tasks.add(new Task("T",wrongDate1 , wrongDate2, 30));
        TasksOperations taskop = new TasksOperations(FXCollections.observableArrayList(tasks.getAll()));
        SortedMap<Date, Set<Task>> calendar = taskop.calendar(start,end);
        assertEquals(0,calendar.size());
    }

    @Test
    @DisplayName("Test case 3")
    void TC_3() {
        ArrayTaskList tasks = new ArrayTaskList() ;
        tasks.add(new Task("T",correctDate1, correctDate2, 30));
        TasksOperations taskop = new TasksOperations(FXCollections.observableArrayList(tasks.getAll()));
        SortedMap<Date, Set<Task>> calendar = taskop.calendar(start,end);
        assertEquals(0,calendar.size());
    }

    @Test
    @DisplayName("Test case 4")
    void TC_4() {
        ArrayTaskList tasks = new ArrayTaskList() ;
        tasks.add(new Task("T",correctDate1, endBefore, 30));
        TasksOperations taskop = new TasksOperations(FXCollections.observableArrayList(tasks.getAll()));
        SortedMap<Date, Set<Task>> calendar = taskop.calendar(start,end);
        assertEquals(0,calendar.size());
    }
}