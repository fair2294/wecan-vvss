package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.services.TasksService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class IntegrateTaskTest {
    private ArrayTaskList arrayTaskList;
    private TasksService tasksService;
    private static Date start;
    private static Date end;
    private static SimpleDateFormat sdf;

    @BeforeEach
    void setUp(){
        sdf = Task.getDateFormat();
        try {
            start = sdf.parse("2020-02-30 12:00");
            end = sdf.parse("2020-02-03 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
        arrayTaskList = new ArrayTaskList();
        Task task = new Task("task", start, end,1000 );
        task.setActive(true);
        arrayTaskList.add(new TaskStub());
        tasksService = new TasksService(arrayTaskList);
    }
    @Test
    void integrateTask_getObservableList(){
        assertEquals(1, arrayTaskList.getAll().size());
    }

    @Test
    void integrateTask_getIntervalInHours(){
        assertEquals("00:16", tasksService.getIntervalInHours(tasksService.getObservableList().get(0)));
    }

}
