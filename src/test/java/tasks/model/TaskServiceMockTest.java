package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.services.TasksService;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class TaskServiceMockTest {
    private ArrayTaskList arrayTaskList;
    private TasksService tasksService;

    @BeforeEach
    void setUp(){
        arrayTaskList= mock(ArrayTaskList.class);
        tasksService=new TasksService(arrayTaskList);
    }

    @Test
    void taskService_getObservableList(){
        Task taskMock1= mock(Task.class);
        Task taskMock2= mock(Task.class);
        Task taskMock3= mock(Task.class);
        Task taskMock4= mock(Task.class);
        Task taskMock5= mock(Task.class);

        Mockito.when(arrayTaskList.getAll()).thenReturn(Arrays.asList(taskMock1, taskMock2, taskMock3, taskMock4, taskMock5));
        assertEquals(5, tasksService.getObservableList().size());
    }

    @Test
    void taskService_getIntervalInHours(){
        Task taskMock = mock(Task.class);

        Mockito.when(arrayTaskList.getAll()).thenReturn(Collections.singletonList(taskMock));
        assertEquals("00:00", tasksService.getIntervalInHours(tasksService.getObservableList().get(0)));
    }
}
