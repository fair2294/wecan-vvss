package tasks.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.fail;

public class TaskStub extends Task {

    private static SimpleDateFormat sdf;

    public TaskStub() {
        sdf = Task.getDateFormat();
        try {
            this.start = sdf.parse("2020-02-30 12:00");
            this.end = sdf.parse("2020-02-03 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
        this.title="task";
        this.interval=1000;
    }
}
